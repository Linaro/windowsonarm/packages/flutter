#!/usr/bin/env bash

set -euo pipefail
set -x

script_dir=$(dirname $(readlink -f $0))

die()
{
    echo "$@" >&2
    exit 1
}

[ $# -eq 2 ] || die "usage: version out_dir"
version=$1; shift
out_dir=$1; shift

setup_environment()
{
    # retrieve Depot tools
    # https://commondatastorage.googleapis.com/chrome-infra-docs/flat/depot_tools/docs/html/depot_tools_tutorial.html#_setting_up
    if [ ! -d depot_tools ]; then
        mkdir -p depot_tools
        pushd depot_tools
        wget -q https://storage.googleapis.com/chrome-infra/depot_tools.zip
        unzip -q -o depot_tools.zip
        popd
    fi

    # cipd_bin holds ninja.exe and other windows binaries
    export PATH="$(pwd)/depot_tools/.cipd_bin:$(pwd)/depot_tools:$PATH"

    # force to get local toolchain (not in Google)
    export DEPOT_TOOLS_WIN_TOOLCHAIN=0

    #export GYP_MSVS_VERSION=2019
    export GYP_MSVS_OVERRIDE_PATH='C:\Program Files (x86)\Microsoft Visual Studio\2019\Community'
}

checkout()
{
    if [ ! -d src ]; then
        cmd.exe /c "fetch flutter"
        pushd src
        git reset --hard $version
        git log -n1
        popd
    fi
}

build_engine()
{
    # https://github.com/flutter/flutter/wiki/Compiling-the-engine
    pushd src
    # gn quick guide: https://chromium.googlesource.com/chromium/src/tools/gn/+/48062805e19b4697c5fbd926dc649c78b6aaa138/docs/quick_start.md#Print-debugging

    # we set local engine name as if it was "native" build
    local local_engine_name=host_debug

    cat > build.bat << EOF
set __VSCMD_PREINIT_PATH=%PATH%
call vcvarsall.bat /clean_env || exit 1
call "c:/Program Files (x86)/Microsoft Visual Studio/2019/Community/VC/Auxiliary/Build/vcvarsall.bat" x64_arm64 || exit 1
@echo on
python3.exe ./flutter/tools/gn --no-goma --windows-cpu arm64 --target-dir $local_engine_name || exit 1
ninja.exe -C out/$local_engine_name || exit 1
EOF
    cmd /c build.bat

    file out/$local_engine_name/*dll out/$local_engine_name/*exe

    popd
}

build_app()
{
    local flutter_engine_path="$1"

    mkdir app
    pushd app

    # flutter main repo holds script to compile/run
    git clone https://github.com/flutter/flutter
    pushd flutter
    git stash
    cat > patch << EOF
diff --git a/packages/flutter_tools/lib/src/windows/build_windows.dart b/packages/flutter_tools/lib/src/windows/build_windows.dart
index c38cf12df2..b25a06bf6c 100644
--- a/packages/flutter_tools/lib/src/windows/build_windows.dart
+++ b/packages/flutter_tools/lib/src/windows/build_windows.dart
@@ -132,6 +132,8 @@ Future<void> _runCmakeGeneration({
         buildDir.path,
         '-G',
         generator,
+        '-A',
+        'ARM64',
       ],
       trace: true,
     );
EOF
    patch -p1 < patch
    popd

    # gallery is an example of application
    git clone https://github.com/flutter/gallery
    export PATH=$(pwd)/flutter/bin:$PATH

    # where we built flutter engine

    flutter_path=$(cygpath -w $(pwd)/flutter/bin/flutter.bat)
    flutter_cmd="$flutter_path --local-engine-src-path=$(cygpath -w "$flutter_engine_path") --local-engine host_debug"
    cmd.exe /c "$flutter_cmd --help"

    # "flutter doctor" to see if installation is fine!
    cmd.exe /c "$flutter_cmd doctor"

    cd gallery
    cmd.exe /c "$flutter_cmd build windows --debug -v"
    echo "final app is available at:"
    file $(pwd)/build/windows/runner/Debug/gallery.exe
    popd
}

setup_environment
checkout
build_engine
# do not build app for now (broken)
#build_app $(pwd)/src
